const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const path = require('path');
const fs = require('fs');

app.use('/form', express.static(path.join(__dirname, '/index.html')));
app.use('/index.css', express.static(`${__dirname}/index.css`));
app.use('/js', express.static(path.join(__dirname, '/src')));
app.use('/files', express.static(path.join(__dirname, 'uploads')));

// default options
app.use(fileUpload());

app.post('/ping', function(req, res) {
  res.send('pong');
});

app.get('/list', function(req, res) {
  const route = path.join(__dirname, '/uploads');
  fs.readdir(route, function(err, file) {
    if (err) {
      return res.status(500).send(err);
    }

    res.send(JSON.stringify(file));
  });
});

app.post('/upload', function(req, res) {
  let sampleFile = null;
  let uploadPath = null;

  if (Object.keys(req.files).length === 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

  sampleFile = req.files.sampleFile; // eslint-disable-line

  uploadPath = path.join(__dirname, '/uploads/', sampleFile.name);

  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }

    res.send(path.join('File uploaded to ', uploadPath));
  });
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000'); // eslint-disable-line
});
