const progress = (function() {
  const downloadProgress = document.querySelector('#downloadProgress');
  const uploadProgress = document.querySelector('#uploadProgress');
  const percentIndicator = document.querySelector('#percentIndicator');
  return {
    onUploadProgress(event) {
      const percent = Math.round((event.loaded * 100) / event.total);
      uploadProgress.value = percent;

      percentIndicator.innerText = `${percent}%`;
    },
    onDownloadProgress(event) {
      const percent = (event.loaded * 100) / event.total;
      downloadProgress.value = percent;
    }
  };
})();
