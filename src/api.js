(function() {
  class Api {
    req = null;
    constructor(url, mainHeaders) {
      this.req = new HttpRequest({ baseUrl: url, headers: mainHeaders });
    }

    getList() {
      return this.req.get('/list', {
        responseType: 'list'
      });
    }

    uploadFile(config) {
      return this.req.post('/upload', config);
    }

    downloadFile(url, config) {
      return this.req.get(url, config);
    }
  }

  window.api = Api;
})();
