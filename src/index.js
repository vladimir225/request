(function() {
  const fileName = document.querySelector('#downloadInput');
  const getUploadForm = document.getElementById('uploadForm');
  const getDownloadForm = document.getElementById('downloadForm');
  const myHeaders = new Headers();

  const List = window.list;
  const Api = window.api;
  const UploadForm = window.uploadForm;
  const DownloadForm = window.downloadForm;
  const fileActions = window.fileActions;

  const api = new Api('http://localhost:8000', myHeaders);
  const list = new List(fileName, api);
  const uploadForm = new UploadForm(getUploadForm, api, list.renderList);
  const downloadForm = new DownloadForm(getDownloadForm, api, fileActions);
})();
