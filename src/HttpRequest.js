class HttpRequest {
  constructor({ baseUrl, headers }) {
    this.baseUrl = baseUrl;
    this.headers = headers;
  }

  _getUrl = (url, params) => {
    const fullUrl = new URL(url, this.baseUrl);
    if (params) {
      for (const key in params) {
        fullUrl.searchParams.set(key, params[key]);
      }
    }

    return fullUrl;
  };

  _promiseRequest = (method, url, config) => {
    const {
      transformResponse,
      headers,
      params,
      responseType,
      data,
      onUploadProgress,
      onDownloadProgress
    } = config;

    const fullUrl = this._getUrl(url, params);

    const xhr = new XMLHttpRequest();
    xhr.open(method, fullUrl);
    xhr.responseType = responseType || 'json';
    xhr.onprogress = onDownloadProgress;
    xhr.upload.onprogress = onUploadProgress;

    const allHeaders = { ...this.headers, ...headers };

    for (const key in headers) {
      xhr.setRequestHeader(key, allHeaders[key]);
    }
    xhr.send(data);

    return new Promise((resolve, reject) => {
      xhr.onload = () => {
        if (xhr.status !== 200) {
          const err = new Error(`${xhr.status}: ${xhr.statusText}`);

          return reject(err);
        }
        if (transformResponse) {
          const responseData = transformResponse.reduce(
            (acc, func) => func(acc),
            xhr.response
          );
          return resolve(responseData);
        }
        return resolve(xhr.response);
      };

      xhr.onerror = () => {
        const err = new Error(`${xhr.status}: ${xhr.statusText}`);

        return rej(err);
      };
    });
  };

  get(url, config) {
    return this._promiseRequest('GET', url, config);
  }

  post(url, config) {
    return this._promiseRequest('POST', url, config);
  }
}

/*
const reuest = new HttpRequest({
  baseUrl: 'http://localhost:3000',
});

reuest.get('/user/12345', { onDownloadProgress, headers: {contentType: undefined} })
  .then(response => {
    console.log(response);
  })
  .catch(e => {
    console.log(e)
  });

reuest.post('/save', { data: formdata, header, onUploadProgress })
  .then(response => {
    console.log(response);
  })
  .catch(e => {
    console.log(e)
  });

config = {

  // `transformResponse` allows changes to the response data to be made before
  // it is passed to then/catch
  transformResponse: [function (data) {
    // Do whatever you want to transform the data

    return data;
  }],

  // `headers` are custom headers to be sent
  headers: {'X-Requested-With': 'XMLHttpRequest'},

  // `params` are the URL parameters to be sent with the request
  // Must be a plain object or a URLSearchParams object
  params: {
    ID: 12345
  },

  // `data` is the data to be sent as the request body
  // Only applicable for request methods 'PUT', 'POST', and 'PATCH'
  // When no `transformRequest` is set, must be of one of the following types:
  // - string, plain object, ArrayBuffer, ArrayBufferView, URLSearchParams
  // - Browser only: FormData, File, Blob
  // - Node only: Stream, Buffer

  data: {
    firstName: 'Fred'
  },

  // `responseType` indicates the type of data that the server will respond with
  // options are 'arraybuffer', 'blob', 'document', 'json', 'text', 'stream'
  responseType: 'json', // default

  // `onUploadProgress` allows handling of progress events for uploads
  onUploadProgress: function (progressEvent) {
    // Do whatever you want with the native progress event
  },

  // `onDownloadProgress` allows handling of progress events for downloads
  onDownloadProgress: function (progressEvent) {
    // Do whatever you want with the native progress event
  },
}
*/
