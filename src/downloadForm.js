class DownloadForm {
  constructor(node, api, callback) {
    this.node = node;
    this.api = api;
    this.callback = callback;
    this.download();
  }

  download() {
    this.node.onsubmit = event => {
      const fileName = document.querySelector('#downloadInput');
      event.preventDefault();
      this.api
        .downloadFile(`files/${fileName.value}`, {
          responseType: 'blob',
          onDownloadProgress: progress.onDownloadProgress
        })
        .then(data => {
          this.runCb(data, fileName.value);
          this.deleteText(fileName);
        });
    };
  }

  runCb(data, fileName) {
    this.callback(data, fileName);
  }

  deleteText(fileName) {
    fileName.value = '';
  }
}

window.downloadForm = DownloadForm;
