class List {
  constructor(node, api) {
    this.node = node;
    this.api = api;
    this.updateList();
  }

  updateList() {
    this.api.getList().then(data => {
      this.renderList(JSON.parse(data));
    });
  }

  renderList(data) {
    const list = document.querySelector('#list');
    data.forEach(item => {
      const createLi = document.createElement('li');
      createLi.classList.add('listItem');
      createLi.innerText = item;
      list.appendChild(createLi);
      createLi.addEventListener('click', () => {
        this.node.value = item;
      });
    });
  }
}

window.list = List;
