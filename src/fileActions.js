const fileActions = function(file, fileName) {
  const url = window.URL.createObjectURL(file);
  if (file.type.includes('image')) {
    renderImage(url);
  } else {
    downloadFile(url, fileName);
  }

  function renderImage(url) {
    const checkImage = document.querySelector('#image');
    if (checkImage) {
      checkImage.remove();
    }

    const createImg = document.createElement('img');
    const image = document.querySelector('.content').appendChild(createImg);
    image.src = url;
    image.id = 'image';
  }

  function downloadFile(url, fileName) {
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    a.click();
  }

  return {
    renderImage,
    downloadFile
  };
};

window.fileActions = fileActions;
