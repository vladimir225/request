class UploadForm {
  constructor(node, api, cb) {
    this.node = node;
    this.api = api;
    this.callback = cb;
    this.upload();
  }

  upload() {
    this.node.onsubmit = event => {
      event.preventDefault();
      const form = new FormData();
      const myHeaders = new Headers();
      myHeaders.append('Content-Type', 'multipart/form-data');
      form.append('sampleFile', event.target.sampleFile.files[0]);
      this.api
        .uploadFile({
          data: form,
          onUploadProgress: progress.onUploadProgress
        })
        .then(() => {
          this.runCb([event.target.sampleFile.files[0].name]);
        });
    };
  }
  runCb(name) {
    this.callback(name);
  }
}

window.uploadForm = UploadForm;
